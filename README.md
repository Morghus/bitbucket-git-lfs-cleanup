# Bitbucket Git LFS Cleanup script

Cleans up Git LFS files that are not currently referenced by the heads of the repository.

## Usage

1. Copy the file `.env-sample` to `.env`.
1. Go to [bitbucket.org](https://bitbucket.org), login and copy your `cloud.session.token` cookie into the SESSION_TOKEN variable in `.env`
1. Execute `npm start [git directory]`

__Warning__: This will delete LFS files from your directory, please use with care and make sure you have a backup in case things go wrong.
