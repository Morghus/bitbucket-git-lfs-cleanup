import fetch, { Headers } from "node-fetch";
import parser from "node-html-parser";
import execa from "execa";
import dotenv from "dotenv";
import { argv } from "process";
import byteSize from "byte-size";
import path from 'path';
dotenv.config();
if (!argv[2]) {
  console.error("Usage: npm start [Git directory]")
  process.exit(1);
}
process.chdir(path.join(process.cwd(), argv[2]));

const { parse } = parser;

const sessionToken = process.env.SESSION_TOKEN;
const gitListFiles = process.env.GIT_LFS_LIST_FILES;

async function getRepository() {
  process.stdout.write(`Getting repository... `);
  const { stdout, stderr, exitCode } = await execa(
    `git remote -v show -n origin`
  );
  if (exitCode !== 0) {
    throw new Error(`Could not get repository info: ${exitCode} ${stderr}`);
  }
  const result = /\b@bitbucket.org:(.*?)\/(.*?).git\b/.exec(stdout);
  if (!result) {
    throw new Error(`Could not parse repository info: ${stdout}`);
  }
  process.stdout.write(`${result[1]}/${result[2]}\n`);
  return { username: result[1], repository: result[2] };
}

const { username, repository } = await getRepository();

let csrftoken;

async function page(num) {
  const headers = new Headers();
  headers.append("Cookie", `cloud.session.token=${sessionToken}`);

  const response = await fetch(
    `https://bitbucket.org/${username}/${repository}/admin/lfs/file-management/?page=${num}&sort=created&iframe=true&spa=0`,
    { headers, redirect: "manual" }
  );
  if (response.status === 404) {
    return false;
  }
  if (!response.ok) {
    throw new Error(
      `Reponse not ok: (${response.status}) ${await response.text()}`
    );
  }

  const text = await response.text();
  const root = parse(text);
  const contents = [];
  for (var el of root.querySelectorAll("[data-lfs-mapping]")) {
    const data = JSON.parse(el.getAttribute("data-lfs-mapping"));
    contents.push(data);
  }
  if (contents.length === 0) {
    return false;
  }
  return contents;
}

async function getRemoteContentIds() {
  process.stdout.write(`Getting remote OIDs... `);
  const parallelism = 10;
  let i = 1;
  const contentIds = new Map();
  let size = 0;
  for (;;) {
    process.stdout.write(
      `\rGetting remote OIDs... ${contentIds.size} found (${byteSize(
        size
      )})    `
    );
    const results = await Promise.all(
      [...Array(parallelism).keys()].map((j) => page(i + j))
    );
    results
      .filter((result) => result !== false)
      .flatMap((result) => result)
      .forEach((data) => {
        size += data.content_size;
        contentIds.set(data.content_hash, data);
      });
    if (results.some((result) => result === false)) {
      break;
    }
    i += parallelism;
  }
  process.stdout.write(
    `\rGetting remote OIDs... ${contentIds.size} found (${byteSize(size)})    `
  );
  process.stdout.write("\n");
  return contentIds;
}

async function getAllBranches() {
  process.stdout.write("Getting branches...");
  const { stdout, stderr, exitCode } = await execa(`git for-each-ref --format "%(refname)"`);
  if (exitCode !== 0) {
    throw new Error("Error: " + stderr);
  }
  const result = stdout
  .split(/\r?\n/);
  return result;
}

async function getLocalContentIds(ref) {
  process.stdout.write(`Getting local OIDs (${ref})...`);
  const { stdout, stderr, exitCode } = await execa(`${gitListFiles} ${ref}`, undefined, {shell: true});
  if (exitCode !== 0) {
    throw new Error("Error: " + stderr);
  }
  const result = stdout
    .split(/\r?\n/)
    .map((line) => /([0-9a-f]+)\s/.exec(line)[1]);
  process.stdout.write(` ${result.length} found\n`);
  return result;
}

async function deleteFile(oid) {
  const headers = new Headers();
  headers.append("Cookie", `cloud.session.token=${sessionToken}`);

  if (!csrftoken) {
    const response = await fetch(
      `https://bitbucket.org/${username}/${repository}/admin`,
      { headers }
    );
    const result = /csrftoken=(.*?)(?:;|$)/.exec(
      response.headers.get("Set-Cookie")
    );
    if (!result) {
      throw new Error("Couldn't get csrftoken");
    }
    csrftoken = result[1];
    console.log("Got CSRFToken: " + csrftoken);
  }

  headers.append(
    "Referer",
    `https://bitbucket.org/${username}/${repository}/admin/lfs/file-management/?sort=created&iframe=true`
  );
  headers.set(
    "Cookie",
    `cloud.session.token=${sessionToken};csrftoken=${csrftoken}`
  );
  headers.append("X-CSRFToken", csrftoken);
  headers.append("X-Bitbucket-Destination", "microsbucket");

  const response = await fetch(
    `https://bitbucket.org/!api/internal/repositories/${username}/${repository}/lfs/${oid}`,
    { headers, method: "DELETE" }
  );
  if (response.status === 204) {
    return;
  }
  throw new Error("Reponse not ok: " + (await response.text()));
}

if (!gitListFiles) {
  console.error(
    "Specify a valid GIT_LFS_LIST_FILES command. Example: git lfs ls-files -l"
  );
  process.exit(1);
}
if (!sessionToken) {
  console.error("Specify a vaid SESSION_TOKEN. Go to the bitbucket.org website, login and use the value of the cloud.session.token cookie.")
}

const branches = await getAllBranches();
let localContentIds = [];
for (let branch of branches) {
    localContentIds.push(...(await getLocalContentIds(branch)));
}
const remoteContentIds = await getRemoteContentIds();

const contentIdsToDelete = new Set(
  [...remoteContentIds.values()].filter(
    (data) => !localContentIds.includes(data.content_hash)
  )
);
console.log(
  `Found ${contentIdsToDelete.size} OIDs to delete (${byteSize(
    [...contentIdsToDelete.values()]
      .map((o) => o.content_size)
      .reduce((a, b) => a + b, 0)
  )})`
);

let i = 1;
for (let data of contentIdsToDelete) {
  console.log(
    `[${i++}/${contentIdsToDelete.size}] Deleting OID: ${data.content_hash}`
  );
  await deleteFile(data.content_hash);
}
